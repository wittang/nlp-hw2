#Wilson Tang
#107513801
#CSE390

import sys
import math

#Process and read file to return sentences
def preprocess(file):
	fileString = open(file).read()
	sentences = fileString.splitlines()
	for index in range(len(sentences)):
		sentences[index] = sentences[index].split('\t')[1]
	return sentences

#Return the word from a tagged word
def get_Word(taggedWord, emissions, tags):
	word_Tag = taggedWord.split("/")
	word = word_Tag[0]
	if "\/" in taggedWord:
		word = word_Tag[0][:-1] + "\/" + word_Tag[1]
	for tag in tags:
		if tag + " " + word in emissions:
			return word
	return "unk"

#Create necessary dictionaries to run program
def setup_Dict(file, smoothing):
	result_Dict = {}
	rows = open(file).read().splitlines()
	for row in rows:
		data = row.split('\t')
		item1 = data[0]
		item2 = data[1]
		prob = data[2]
		if len(data) > 3:	
			if smoothing == 'L':
				prob = data[3]
		result_Dict[item1 + " " + item2] = float(prob)
	return result_Dict

#Return list of all unique tags
def get_Tags(tagPair_Dict):
	tags = []
	for tagPair in tagPair_Dict:
		tag1 = tagPair.split()[0]
		tag2 = tagPair.split()[1]
		if tag1 not in tags:
			tags.append(tag1)
		if tag2 not in tags:
			tags.append(tag2)
	return tags

#Calculate score using previous score, transition probability, and emission probability
def calc_Score(prev_Score, transitions, transition_Key, emissions, emission_Key):
	return prev_Score + get_Transition(transitions, transition_Key) + get_Emission(emissions, emission_Key)

#Return log of transition probability if found, otherwise log of 0.000000000000000000000000000001
def get_Transition(transitions, transition_Key):
	if transition_Key in transitions:
		if transitions[transition_Key] == 0:
			return math.log(0.000000000000000000000000000001)
		return math.log(transitions[transition_Key])
	else:
		return math.log(0.000000000000000000000000000001)

#Return log of emission probability if found, otherwise log of 0.000000000000000000000000000001
def get_Emission(emissions, emission_Key):
	if emission_Key in emissions:
		if emissions[emission_Key] == 0:
			return math.log(0.000000000000000000000000000001)
		return math.log(emissions[emission_Key])
	else:
		return math.log(0.000000000000000000000000000001)

#Update the dynamic programming structure (scores/backpointers)
def update_DPStruct(struct, i, j, value):
	if j == 0:
		struct.append([value])
	else:
		struct[i].append(value)

#Initialization of vertibi algorithm, returns scores and backpointers dynamic programming structures
def vertibi_Init(taggedWord, transitions, emissions, tags):
	scores = []
	backpointers = []
	for index in range(len(tags)):
		word = get_Word(taggedWord, emissions, tags)
		transition_Key = "<s> " + tags[index]
		emission_Key = tags[index] + " " + word
		score = calc_Score(0, transitions, transition_Key, emissions, emission_Key)
		backpointer = 0
		update_DPStruct(scores, 0, index, score)
		update_DPStruct(backpointers, 0, index, backpointer)
	return scores, backpointers

#Forward pass of vertibi algorithm, calculate max scores and index for each word in the sentence
def vertibi_FP(scores, backpointers, transitions, emissions, tags, sentence):
	for i in range(1, len(sentence)):
		for j in range(len(tags)):
			word = get_Word(sentence[i], emissions, tags)
			max_Score = calc_Score(scores[i - 1][0], transitions, tags[0] + " " + tags[j], emissions, tags[j] + " " + word)
			max_BP = 0
			for k in range(len(tags)):
				transition_Key = tags[k] + " " + tags[j]
				emission_Key = tags[j] + " " + word
				score = calc_Score(scores[i-1][k], transitions, transition_Key, emissions, emission_Key)
				if max_Score < score:
					max_Score = score
					max_BP = k
			update_DPStruct(scores, i, j, max_Score) 
			update_DPStruct(backpointers, i, j, max_BP)

#Backtrace using backpointers to determine best tags
def backTrace(scores, backpointers, tags, sentence):
	bestTags = []
	for score in scores:
		best_Score = score[0]
		best_Tag = 0
		for k in range(len(tags)):
			new_Score = score[k]
			if best_Score < new_Score:
				best_Score = new_Score
				best_Tag = k
		bestTags.append(best_Tag)
	for k in range(len(sentence) - 2, -1, -1):
		bestTags[k] = backpointers[k+1][bestTags[k+1]] 
	return bestTags

#Vertibi algorithm, returns best tags as well as sequence probability
def vertibi(sentence, transitions, emissions, tags):
	sentence = sentence.split()
	scores, backpointers = vertibi_Init(sentence[0], transitions, emissions, tags)
	vertibi_FP(scores, backpointers, transitions, emissions, tags, sentence)
	bestTags = backTrace(scores, backpointers, tags, sentence)
	for index in range(len(bestTags)):
		bestTags[index] = tags[bestTags[index]]
	seq_Prob = max(scores[-1])
	return bestTags, seq_Prob

#Main to run program
def main():
	smoothing = sys.argv[4].upper()
	sentences = preprocess(sys.argv[1])
	transitions = setup_Dict(sys.argv[2], smoothing)
	emissions = setup_Dict(sys.argv[3], smoothing)
	tags = get_Tags(transitions)
	output_File = open(sys.argv[5], "w")
	for sentence in sentences:
		bestTags, seq_Prob = vertibi(sentence, transitions, emissions, tags)
		words = sentence.split()
		for index in range(len(words)):
			if index == 0:
			 	output_File.write(str(seq_Prob) + "\t")
			tagged_Word = words[index] + "/" + bestTags[index]
			output_File.write(tagged_Word)
			if index != len(words) - 1:
				output_File.write(" ")
			else:
				output_File.write("\n")
	output_File.close()
		

main()