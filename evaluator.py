import sys

def preprocess(file):
	fileString = open(file).read()
	sentences = fileString.splitlines()
	for index in range(len(sentences)):
		sentences[index] = sentences[index].split('\t')[1]
	return sentences

def dict_update(dict, key):
	if key in dict:
		dict[key] +=1
	else:
		dict[key] = 1

def get_Tags(taggedWord):
	data = taggedWord.split("/")
	#word = data[0]
	test_Tag = data[1]
	system_Tag = data[2]
	if "\/" in taggedWord:
		test_Tag = data[2]
		system_Tag = data[3]
	return test_Tag, system_Tag

def main():
	correct_Predictions = 0
	total_Predictions = 0
	correct_Tag_Dict = {}
	system_Tag_Dict = {}
	test_Tag_Dict = {}
	sentences = preprocess(sys.argv[1])
	for sentence in sentences:
		for taggedWord in sentence.split():
			total_Predictions += 1
			test_Tag, system_Tag = get_Tags(taggedWord)
			if test_Tag == system_Tag:
				correct_Predictions += 1
				dict_update(correct_Tag_Dict, test_Tag)
			dict_update(system_Tag_Dict, system_Tag)
			dict_update(test_Tag_Dict, test_Tag)
	accuracy = float(correct_Predictions)/total_Predictions
	print("ACCURACY:\t" + str(accuracy) + "\n")
	for tag in correct_Tag_Dict:
		print("TAG:      \t" + tag)
		percision = float(correct_Tag_Dict[tag])/system_Tag_Dict[tag]
		recall = float(correct_Tag_Dict[tag])/test_Tag_Dict[tag]
		f1 = float(2*percision*recall)/(percision + recall)
		print("PERCISION:\t" + str(percision))
		print("RECALL:   \t" + str(recall))
		print("F1:       \t" + str(f1) + "\n")

			

main()


