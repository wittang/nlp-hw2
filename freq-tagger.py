#Wilson Tang
#107513801
#CSE390

import sys

#Process and read file to return sentences
def preprocess(file):
	fileString = open(file).read()
	sentences = fileString.splitlines()
	for index in range(len(sentences)):
		sentences[index] = sentences[index].split('\t')[1]
	return sentences

#Return the word from a tagged word
def get_Word(taggedWord):
	wordTag = taggedWord.split("/")
	if "\/" in taggedWord:
		wordTag[0] = wordTag[0][:-1] + "\/" + wordTag[1]
	return wordTag[0]

#Create necessary dictionaries to run program
def setup_Dict(file):
	result_Dict = {}
	rows = open(file).read().splitlines()
	for row in rows:
		data = row.split('\t')
		if len(data) > 2:	
			emiss_Data = row.split('\t')
			tag = emiss_Data[0]
			word = emiss_Data[1]
			result_Dict[tag + " " + word] = [float(i) for i in emiss_Data[2:]]
		else:
			uni_Data = row.split('\t')
			tag = uni_Data[0]
			prob = float(uni_Data[1])
			result_Dict[tag] = prob
	return result_Dict

#Return tag with maximum unigram laplace probability
def get_MaxTag(uni_Dict, tags):
	max_Tag = tags[0]
	for tag in tags:
		if uni_Dict[tag] > uni_Dict[max_Tag]:
			max_Tag = tag
	return max_Tag

#Check if there are probability ties, if so, call get max tag
def get_Tag(uni_Dict, tags_Found):
	max_Tag = max(tags_Found, key = tags_Found.get).split()[0]
	max_Prob = max(tags_Found.values())
	tags = []
	count = 0
	for tag in tags_Found:
		if max_Prob == tags_Found[tag]:
			tags.append(tag.split()[0])
			count += 1
	if count > 1:
		return get_MaxTag(uni_Dict, tags)
	return max_Tag

#Tag the word
def tag_Word(emiss_Dict, uni_Dict, taggedWord):
	word = get_Word(taggedWord)
	tag = max(uni_Dict, key = uni_Dict.get)
	tags_Found = {}
	for key in emiss_Dict:
		if word == key.split()[1]:
			tags_Found[key] = emiss_Dict[key][2]
	if any(tags_Found):
		if len(tags_Found) == 1:
			return taggedWord + "/" + list(tags_Found)[0].split()[0]
		tag = get_Tag(uni_Dict, tags_Found)
	return taggedWord + "/" + tag

#Main to run program
def main():
	emiss_Dict = setup_Dict(sys.argv[2])
	uni_Dict = setup_Dict(sys.argv[3])
	sentences = preprocess(sys.argv[1])
	output_File = open(sys.argv[4], "w")
	for index in range(len(sentences)):
		output_File.write(str(index) + "\t")
		words = sentences[index].split()
		for index in range(len(words)):
			tagged_Word = tag_Word(emiss_Dict, uni_Dict, words[index])
			output_File.write(tagged_Word)
			if index != len(words) - 1:
				output_File.write(" ")
			else:
				output_File.write("\n")
	output_File.close()

main()