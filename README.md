Wilson Tang
107513801
CSE390

Necessary Files:
train-tagger.py
freq-tagger.py
hmm-tagger.py
evaluator.py
train.txt
test.txt

To begin: Open terminal and navigate to location of files

train-tagger.py
To run the tag trainer, in the terminal, enter: python train-tagger.py train.txt [transitions.txt] [emissions.txt] [laplace-tag-unigram.txt]
WITHOUT the brackets. 
train.txt is the training set
The .txt files within brackets will be the names of the output files that contain the transition, emission, and unigram laplace probabilities

freq-tagger.py
To run the frequency tagger program, in the terminal, enter: python freq-tagger.py test.txt emissions.txt laplace-tag-unigram.txt [test-fbtagger.txt]
WITHOUT the brackets
test.txt is the test set
The .txt file within the bracket will be the name of the output file that contains the test set after being tagged by the frequency tagger

hmm-tagger.py
To run the HMM tagger program, in the terminal, enter: python hmm-tagger.py test.txt transitions.txt emissions.txt [M/L] [output.txt]
WITHOUT the brackets. 
test.txt is the test set
The M/L is the smoothing method to be used. M for MLE and L for Laplace
The .txt file within the bracket will be the name of the output file that contains the test set after being tagged by the HMM tagger along with the tag sequence probability

evaluator.py
To run the tagger evaluator program, in the terminal, enter: python evaluator.py [output.txt]
WITHOUT the brackets.
The .txt file has to be one of the output files from either the Frequency Tagger or HMM Tagger.
