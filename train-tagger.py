#Wilson Tang
#107513801
#CSE390

import sys

#Process and read file to return sentences
def preprocess(file):
	fileString = open(file).read()
	sentences = fileString.splitlines()
	for index in range(len(sentences)):
		sentences[index] = "<s> " + sentences[index].split('\t')[1] + " </s>"
	return sentences

#If word exists, increase count. Otherwise, add word to dictionary. 
def dict_update(dict, key):
	if key in dict:
		dict[key] +=1
	else:
		dict[key] = 1

#Return the word and tag from a tagged word
def get_WordTag(taggedWord):
	if taggedWord in ["<s>", "</s>"]:
		return taggedWord, taggedWord
	wordTag = taggedWord.split("/")
	if "\/" in taggedWord:
		wordTag[0] = wordTag[0][:-1] + "\/" + wordTag[1]
		wordTag[1] = wordTag[2]
	return wordTag[0], wordTag[1]

#Return necessary dictionaries to run program
def get_Dicts(sentences):
	tag_Dict = {}
	word_Dict = {}
	tagPair_Dict = {}
	tagWord_Dict = {}
	for sentence in sentences:
		words = sentence.split()
		for index in range(len(words)):
			word, tag = get_WordTag(words[index])
			dict_update(tag_Dict, tag)
			dict_update(word_Dict, word)
			dict_update(tagWord_Dict, tag + " " + word)
			if index < (len(words) - 1):
				word2, tag2, = get_WordTag(words[index + 1])
				dict_update(tagPair_Dict, tag + " " + tag2)
	return tag_Dict, word_Dict, tagPair_Dict, tagWord_Dict

#Calculate transition probability given a tag pair
def calc_Trans(tag_Dict, tagPair_Dict, tagPair):
	tag1 = tagPair.split()[0]
	tag2 = tagPair.split()[1]
	tag1_Count = tag_Dict[tag1]
	tagPair_Count = tagPair_Dict[tagPair]
	prob_MLE = float(tagPair_Count)/tag1_Count
	return tag1, tag2, prob_MLE

#Calculate emission probabilities given tag word pair
def calc_Emiss(tag_Dict, word_Dict, tagWord_Dict, tagWord):
	tag = tagWord.split()[0]
	word = tagWord.split()[1]
	tags_Count = len(tag_Dict)
	tag_Count = tag_Dict[tag]
	word_Count = word_Dict[word]
	tagWord_Count = tagWord_Dict[tagWord]
	prob_MLE_TW = float(tagWord_Count)/tag_Count
	prob_L = float(tagWord_Count + 1)/(tag_Count + tags_Count + 1)
	prob_MLE_WT = float(tagWord_Count)/word_Count
	return tag, word, prob_MLE_TW, prob_L, prob_MLE_WT

#Calculate Unigram Laplace Probability of the tag
def calc_LUni(tag_Dict, tag):
	total_Tags = sum(tag_Dict.values())
	tags_Count = len(tag_Dict)
	tag_Count = tag_Dict[tag]
	prob_L = float(tag_Count + 1)/(total_Tags + tags_Count + 1)
	return prob_L

#Main to run program
def main():
	sentences = preprocess(sys.argv[1])
	tag_Dict, word_Dict, tagPair_Dict, tagWord_Dict = get_Dicts(sentences)

	transitions_File = open(sys.argv[2], "w")
	for tagPair in tagPair_Dict:
		tag1, tag2, prob_MLE = calc_Trans(tag_Dict, tagPair_Dict, tagPair)
		transitions_File.write(tag1 + "\t" + tag2 + "\t" + str(prob_MLE) + "\n")
	transitions_File.close()

	emissions_File = open(sys.argv[3], "w")
	for tagWord in tagWord_Dict:
		tag, word, prob_MLE_TW, prob_L, prob_MLE_WT= calc_Emiss(tag_Dict, word_Dict, tagWord_Dict, tagWord)
		emissions_File.write(tag + "\t" + word + "\t" + str(prob_MLE_TW) + "\t" + str(prob_L) + "\t" + str(prob_MLE_WT) + "\n")
	for tag in tag_Dict:
		prob = float(1)/(tag_Dict[tag] + len(tag_Dict) + 1)
		emissions_File.write(tag + "\t" + "unk" + "\t" + str(0.0) + "\t" + str(prob) + "\t" + str(0.0) + "\n")
	emissions_File.close()

	laplaceUni_File = open(sys.argv[4], "w")
	for tag in tag_Dict:
		prob_L = calc_LUni(tag_Dict, tag)
		laplaceUni_File.write(tag + "\t" + str(prob_L) + "\n")
	laplaceUni_File.close()

main()